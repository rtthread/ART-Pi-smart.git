/*
 * Copyright (c) 2006-2022, RT-Thread Development Team
 *
 * SPDX-License-Identifier: GPL-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-05-23     liukang      The first version
 */

#ifndef __LIBDBKIT_H__
#define __LIBDBKIT_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef struct dbkit* dbkit_t;

dbkit_t dbkit_connect(const char *dbname, int flags);
int dbkit_disconnect(dbkit_t sql);
int dbkit_statement(dbkit_t sql, char* sql_msg);
char* dbkit_getresult(dbkit_t sql);
int dbkit_freeresult(char* result);
char* dbkit_strerror(dbkit_t sql);
int dbkit_rows(char* res);
void *dbkit_cell(char *res, int row, int col);

#ifdef __cplusplus
}
#endif

#endif
