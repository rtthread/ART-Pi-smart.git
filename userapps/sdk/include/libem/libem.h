/*
 * Copyright (c) 2006-2022, RT-Thread Development Team
 *
 * SPDX-License-Identifier: GPL-2.0
 *
 * Change Logs:
 * Date           Author            Notes
 * 2022-05-24     liangjiayuan      The first version
 */
#ifndef LIB_EM_H__
#define LIB_EM_H__
int service_end(char *service_name);

int service_init(char *service_name);

int service_ping();
#endif