/*
 * Copyright (c) 2006-2022, RT-Thread Development Team
 *
 * SPDX-License-Identifier: GPL-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-05-28     liukang      The first version
 */

#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

#include <libdbkit.h>

#define DATABASE_PATH  "/bin/employee.db"

/* just for test */
int main(int argc, char **argv)
{
    dbkit_t hdl = NULL;
    int rc = 0;
    char *errmsg = NULL;
    char *res = NULL;

    hdl = dbkit_connect(DATABASE_PATH, 0); /* connect to database */
    if (hdl == NULL)
    {
      printf("Error connecting to database: %s\n", strerror(errno));
      return EXIT_FAILURE;
    }
    /* create a table into the database */
    rc = dbkit_statement(hdl, "create table employee(staff_id integer PRIMARY KEY, name text, gender text, age integer);");
    if (rc != 0)
    {
        errmsg = dbkit_strerror(hdl);
        printf("Error executing create table statement: %s\n", errmsg);
        return EXIT_FAILURE;
    }
    /* INSERT a row into the database */
    rc = dbkit_statement(hdl, "insert into employee values(1,'LiHua','nan',18);"); 
    if (rc != 0)
    {
        errmsg = dbkit_strerror(hdl);
        printf("Error executing INSERT statement: %s\n", errmsg);
        return EXIT_FAILURE;
    }
    /* INSERT a row into the database */ 
    dbkit_statement(hdl, "insert into employee values(2,'ZhangSan','nan1',19);");
    if (rc != 0)
    {
        errmsg = dbkit_strerror(hdl);
        printf("Error executing INSERT statement: %s\n", errmsg);
        return EXIT_FAILURE;
    }
    /* SELECT one row from the database-- */
    /* this statement combines the first and last names into full names */
    rc = dbkit_statement(hdl, "select * from employee;");
    if (rc != 0)
    {
        errmsg = dbkit_strerror(hdl);
        printf("Error executing INSERT statement: %s\n", errmsg);
        return EXIT_FAILURE;
    }

    /* get the result */
    res = dbkit_getresult(hdl);
    if (res == NULL)
    {
        errmsg = dbkit_strerror(hdl);
        printf("Error executing INSERT statement: %s\n", errmsg);
        return EXIT_FAILURE;
    }
    /* return the number of rows in a result set */
    if (dbkit_rows(res) == 2) 
    {
        /* return a cell's data */
        printf("Got a customer's full name: %s\n", (char *)dbkit_cell(res, 0, 1)); /* need return LiHua */
    }
    else 
    {
        printf("No customers in the database!\n");
    }

    /* free the result */
   rc = dbkit_freeresult(res);
   if (rc != 0)
   {
      fprintf(stderr, "Error freeing SQL statement results: %s\n", strerror(errno));
      return EXIT_FAILURE;
   }
    /* disconnect from the server */
    rc = dbkit_disconnect(hdl);
    if (rc != 0)
    {
        fprintf(stderr, "Error disconnecting from database: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
