/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: GPL-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-30     linzhenxing      The first version
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <lwp_shm.h>
#include <rtthread.h>

#include "ulogd.h"
/**
 * With separate address spaces, we transfer the id of the shared-memory page
 * which contains the string, instead of the pointer to the string directly.
 */
rt_inline int prepare_data(void *data, size_t len)
{
    int shmid;
    void *shm_vaddr;

    /* use the current thread ID to label the shared memory */
    size_t key = (size_t) rt_thread_self();
    shmid = lwp_shmget(key, len, 1);    /* create a new shm */
    if (shmid == -1)
    {
        printf("Fail to allocate a shared memory!\n");
        return -1;
    }

    /* get the start address of the shared memory */
    shm_vaddr = lwp_shmat(shmid, NULL);
    if (shm_vaddr == RT_NULL)
    {
        printf("The allocated shared memory doesn't have a valid address!\n");
        lwp_shmrm(shmid);
        return -1;
    }

    /* put the data into the shared memory */
    memcpy(shm_vaddr, data, len);
    lwp_shmdt(shm_vaddr);

    return shmid;
}
#define RT_NAME_LENGTH 50
#define RT_DATA_LENGTH 300
struct rt_ulog_msg
{
    int ulog_lvl;
    char process_name[RT_NAME_LENGTH];
    char func[RT_NAME_LENGTH];
    char data[RT_DATA_LENGTH];
};

void ulog_printf(int ulog_lvl, char *process_name, char *func, char *data)
{
    int ulog_ch;
    /* channel messages to send and return back */
    struct rt_channel_msg ch_msg, ch_msg_ret;
    struct rt_ulog_msg ulog_msg ;

    if(strlen(process_name) >= RT_NAME_LENGTH || strlen(func) >= RT_NAME_LENGTH || strlen(data) >= RT_DATA_LENGTH)
    {
        printf("usage:process_name-func-data don't more than 50-50-300 sizes\n");
        return;
    }
    /* open the IPC channel created by 'ulogd' */
    ulog_ch = rt_channel_open("ulogd", 0);
    if (ulog_ch == -1)
    {
        printf("Error: rt_channel_open: could not find the \'ulogd\' channel!\n");
        return;
    }

    /* try to communicate through the IPC channel */
    ulog_msg.ulog_lvl = ulog_lvl;
    strcpy(ulog_msg.process_name, process_name);
    strcpy(ulog_msg.func, func);
    strcpy(ulog_msg.data, data);

    ch_msg.type = RT_CHANNEL_RAW;

    int shmid = prepare_data(&ulog_msg, sizeof(ulog_msg));
    if (shmid < 0)
    {
        printf("Ping: fail to prepare the ping message.\n");
        return;
    }

    ch_msg.u.d = (void *)(size_t)shmid;
    rt_channel_send_recv(ulog_ch, &ch_msg, &ch_msg_ret);
    lwp_shmrm(shmid);

    /* get rid of the channel and the shared memory */
    rt_channel_close(ulog_ch);
}

