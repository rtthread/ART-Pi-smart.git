/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-07-30     linzhenxing       the first version
 */

#ifndef _ULOGD_H_
#define _ULOGD_H_

/* logger level, the number is compatible for ulogd */
#define ULOGD_LVL_ASSERT                 0
#define ULOGD_LVL_ERROR                  3
#define ULOGD_LVL_WARNING                4
#define ULOGD_LVL_INFO                   6
#define ULOGD_LVL_DBG                    7

void ulog_printf(int ulog_lvl, char *process_name, char *func, char *data);

#define ULOG_A(process_name, func, data) ulog_printf(ULOGD_LVL_ASSERT, process_name, func, data)
#define ULOG_E(process_name, func, data) ulog_printf(ULOGD_LVL_ERROR, process_name, func, data)
#define ULOG_W(process_name, func, data) ulog_printf(ULOGD_LVL_WARNING, process_name, func, data)
#define ULOG_I(process_name, func, data) ulog_printf(ULOGD_LVL_INFO, process_name, func, data)
#define ULOG_D(process_name, func, data) ulog_printf(ULOGD_LVL_DBG, process_name, func, data)
#endif /* _ULOGD_H_ */
