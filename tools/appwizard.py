import os
import shutil

from ci import CI

def sdk_detect_path():
    path = os.getenv('SDK_ROOT') or '.'
    if path == '.': 
        # should detect SDK ROOT
        pass

    return path

def sdk_create_app(app_name):
    app_path = os.path.join(os.path.abspath('.'), app_name)
    if os.path.exists(app_path):
        print('Warning: %s folder exists.' % app_name)
        exit(-1)

    sdk_path = sdk_detect_path()
    if sdk_path == '.':
        print('Warning: not found sdk root.')
        exit(-1)

    template_path = os.path.join(sdk_path, 'userapps','sdk', 'template', 'hello')

    # make application
    os.makedirs(app_path)
    ci = CI()

    ci.copyTree(os.path.join(template_path), app_path)

    return

def sdk_generate():
    app_path = os.path.abspath('.')
    if not os.path.exists(os.path.join(app_path, 'SConscript')):
        print('Warning: not found SConscript for application')
        exit(-1)
    app_name = os.path.basename(app_path)

    sdk_path = sdk_detect_path()
    if sdk_path == '.':
        print('Warning: not found sdk root.')
        exit(-1)

    uroot_path = os.path.relpath(os.path.join(sdk_path, 'userapps'), app_path).replace('\\', '/')
    template_path = os.path.join(sdk_path, 'userapps','sdk', 'template', 'hello')
    ci = CI()

    # copy .vscode
    ci.touchDir(os.path.join(app_path, '.vscode'))
    vscode_files = ['launch.json', 'settings.json', 'smart.json', 'tasks.json']
    for item in vscode_files:
        src_path = os.path.join(template_path, '.vscode', item)
        dst_path = os.path.join(app_path, '.vscode', item)

        shutil.copyfile(src_path, dst_path)

    # generate settings.json
    if True: # try :
        data = open(os.path.join(template_path, '.vscode', 'settings.json')).read()
        data = data.format(name=app_name)

        sconstruct = open(os.path.join(app_path, '.vscode', 'settings.json'), 'w')
        sconstruct.write(data)
        sconstruct.close()

    # generate SConscript
    try :
        data = open(os.path.join(template_path, 'SConscript')).read()
        data = data.format(name=app_name)

        sconstruct = open(os.path.join(app_path, 'SConscript'), 'w')
        sconstruct.write(data)
        sconstruct.close()
    except:
        print('Warning: write SConscript failed.')
        exit(-1)

    # generate SConstruct
    try :
        data = open(os.path.join(template_path, 'SConstruct')).read()
        data = data.format(name=app_name, uroot=uroot_path)

        sconstruct = open(os.path.join(app_path, 'SConstruct'), 'w')
        sconstruct.write(data)
        sconstruct.close()
    except:
        print('Warning: write SConstruct failed.')
        exit(-1)

    return

if __name__ == '__main__':
    print('RT-Thread Smart App Wizard')

    # sdk_create_app('test')
    sdk_generate()
